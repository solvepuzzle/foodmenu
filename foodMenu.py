#coding=utf-8
import sys
import requests
import xlrd
import xlwt
import re
from bs4 import BeautifulSoup

# diable https waring and then get session
requests.packages.urllib3.disable_warnings()
rs = requests.session()

# 進入的主頁面以及食物種類的URL
mainURL="http://www.ipeen.com.tw"
typeURL=[]


# 將所有食物種類的URL儲存在typeURL tuple裡
def getAllType(foodURL):
    res = rs.get(foodURL, verify=False)
    soup = BeautifulSoup(res.text, 'html.parser')
    for tag in soup.select('.a37.ga_tracking'):
        if(tag['data-action'] == "food_navigation"):
            typeURL.append(tag['href'])
    
# 透過食物種類每一頁的URL(foodTypeURL)得到店家清單
def getAllStore(foodTypeURL):
    res = rs.get(foodTypeURL, verify=False)
    soup = BeautifulSoup(res.text, 'html.parser')
    for tag in soup.select('li.score')[1:]:
        atag = tag.find('a')
        # 如果有<a>,則可以進入店家
        if(atag):
            hyphen = atag['href'].find('-')  # 得到'-'的index
            storeId = atag['href'][6:hyphen] # 每個店家的URL由/shop/+id+店名組成,因此只取id就好    
            getMenu(mainURL+atag['href'], storeId)
    for nextPage in soup.select(u'.ga_tracking'):
        if nextPage['data-label'] == u'下一頁':
            return nextPage['href']

# 得到menu資訊
def getMenu(storeURL, storeId):
    res = rs.get(storeURL, verify=False)
    soup = BeautifulSoup(res.text, 'html.parser')
    menu_tag = soup.find('a', {'data-label': u'簡介菜單'})

    # 如果該店家有menu的tag
    if menu_tag:
        menu_readable = soup.find_all('a', {'href': '/shop/' + (storeId) + '/menu'})

        # 必須有兩個menu的tag.第一個menu的tag為顯示菜單按鈕,第二個則表示按下菜單按鈕後會有菜單出現
        if len(menu_readable)>1:
            menuURL = mainURL + menu_tag['href']
            res = rs.get(menuURL, verify=False)
            soup = BeautifulSoup(res.text, 'html.parser')
            print(storeId+'\n')
            writeMenuToExcel(soup)

# 將menu資料寫到         
def writeMenuToExcel(soup):
    menuList = soup.find_all('section', {'class': 'group none-pic'})
    for block in menuList:
        items = block.find_all('td')
        for item in items:
#            pass
            print(re.split(r'[\ \n\t]+',item.text.strip()))

if __name__ == "__main__":
    foodURL="http://www.ipeen.com.tw/taiwan/channel/F"

    getAllType(foodURL)

    # 搜尋所有食物種類
    for fTypeUrl in typeURL:
        searchURL = mainURL+fTypeUrl
        nextPageUrl = getAllStore(searchURL)
        while nextPageUrl:
            nextPageUrl = getAllStore(nextPageUrl)    
    
